#!/usr/bin/env python
"""
Contuner of POST calls

Usage::
    ./counterservice.py [<port>]

Send a POST request:
    curl -X POST http://localhost

"""
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer

class Counter(BaseHTTPRequestHandler):
    counter=1   

class CounterServer(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        
    def do_POST(self):
        self._set_headers()
        self.wfile.write("<html><body><h1>Counter: "+ str(Counter.counter) +"</h1></body></html>")
        Counter.counter+=1
        
def run(server_class=HTTPServer, handler_class=CounterServer, port=9000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
